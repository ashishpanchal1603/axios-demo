import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://6364aace7b209ece0f4adfbb.mockapi.io/'
})

export default instance
